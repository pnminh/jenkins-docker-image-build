Source is From [how to install docker on centos 6](https://www.liquidweb.com/kb/how-to-install-docker-on-centos-6/)    
In official document, docker need linux kernel up to 3.10.x    
So we use this way to install docker on centos
# Add the EPEL Repository    
Docker is part of Extra Packages for Enterprise Linux (EPEL), which is a community repository of non-standard packages for the RHEL distribution. First, we’ll install the EPEL repository:
```
rpm -iUvh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
```
Then, as a matter of best practice, we’ll update our packages:
```
yum update -y
```    
# Installation
Installation
Now let’s install Docker by installing the docker-io package:
```
yum -y install docker-io
```
Once the installation completes, we’ll need to start the Docker daemon:
```
service docker start
```
And finally, and optionally, let’s configure Docker to start when the server boots:
```
chkconfig docker on
```

**Docker is now installed.**
