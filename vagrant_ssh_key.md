Steps to run vagrant using ssh key with mRemoteNg:
1. Get private_key location
```
vagrant ssh-config
```
2. Use puttyGen to generate ppk
3. From mRemoteNg go to Options/Advanced and select Launch putty
4. From putty go to SSH>Auth and select the ppk private key, save the profile
5. Create a connection from mRemoteNg and use the putty profile just created
