The repo to build jenkins pipeline to create docker image
#.gitignore
To remove files from .gitignore but have already been committed:
```
  git -rm --cached [file-name]
```
Or if we want to remove the directory from the commit:
```
 git -rm -r --cached [dir-name]
```
#Build VM
- Create Vagrantfile
- Install docker:
```
https://docs.docker.com/engine/installation/linux/docker-ce/centos/
```
- Run docker daemon with options (json file included):
```
nano /etc/docker/daemon.json
```
- Check if tcp port is open:
```
sudo yum install net-tools
netstat -nplt
```

#RUN docker-compose
1. install docker-compose:
```
curl -L https://github.com/docker/compose/releases/download/1.14.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```
2. docker-compose has to be run as root, so we need to add /usr/local/bin (or any new path) to the secure path option in /etc/sudoers
```
sudo nano /etc/sudoers
```
File /etc/sudoers:
```
...
Defaults    secure_path = /sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin
...
```
Or we can run "su -" and use root ~/.bash_profile to add new path
3. Run docker-compose build

#Sync folder with vagrant
if using rsync
```
vagrant rsync-auto
```

#docker Cheatsheet
remove all containers (force)
```
docker rm -f $(docker ps -a -q)
```

#Jenkins cheatsheet
1. Create Jenkins slave

On Master (jenkins host):  
- Create ssh key pairs
```
cd /home/${username}/.ssh && ssh-keygen
```
- chmod ssh to make sure we have valid permission (if we use this master to ssh, in case we use jenkins we need to configure on jenkin server)
```
chmod 700 ~/.ssh
```
- Copy content from id_rsa.pub (to be pasted to /home/${jenkins_slave_user}/.ssh/authorized_keys
- Copy content from id_rsa to be used on jenkins server setup.  
On Slave:  
- Create slave jenkins user ${jenkins_slave_user.
- Create root directory for jenkins to keep workspace data.
- Paste content from the master id_rsa.pub to /home/${jenkins_slave_user}/.ssh/authorized_keys.
- chmod ssh to make sure we have valid permission:
```
chmod 600 ~/.ssh/authorized_keys
```  
On Jenkins: by convention, the jenkins_home is /var/lib/jenkins.  
- Go to the configuration of credentials under Manage Jenkins/Manage Credentials/Add Credentials ( JENKINS_URL/credential-store/) and choose SSH username with private key.
![alt text](https://cloudbees.zendesk.com/hc/article_attachments/209464428/sshSlaveCrendetialsConfig.png-c5930503)
- For every slave that you want to connect using the user $SLAVE_USER, configure the slave under Manage Nodes/$NODE_NAME/configure ( $JENKINS_URL/computer/$NODE_NAME/configure) and use this user credential.
![alt text](https://cloudbees.zendesk.com/hc/article_attachments/209464448/node-config.png-18d4b3a0
node-config)
- if jenkins complains that the host is not added to the knownhosts file, log in as jenkins user on the master node and ssh to the host that is not on the knownhosts file. This will add the host public key to the knownhosts.
Note: Using that configuration, only one credential per user can be used. Several slaves can be launched for the same user on the same machine without any further configuration. To connect slaves hosted on a different machine using the same credential, the same SSH public Key needs to be added in the authorized_keys logged as $SLAVE_USER